Solução desenvolvida utilizando a linguagem Ruby.

Para executar o código:
```bash
ruby numeros-romanos/main.rb
```

Para os testes unitários:

```bash
cd numeros-romanos/tests
rspec main_spec.rb
```

> É necessário ter o rspec instalado: `gem install rspec`


Evidência dos 1000 cenários de testes passando:

![Print da evidencia](evidencia_testes_unitarios.png)

Alunos: André Prolo, Gustavo Daniel Unser e Luiz Valdameri.