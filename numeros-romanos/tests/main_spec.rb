require_relative '../main.rb'

def read_tests_file
    result = {}

    File.readlines('test_cases.txt').each do |line|
        line.strip!
        result[(line.split(";")[1])] = line.split(";")[0].to_i
    end

    result
end

describe 'Testing test_cases.txt' do
    test_cases = read_tests_file

    test_cases.keys.each_with_index do |value, index| 
        it "running value #{value} expecting #{test_cases[value]} as return" do
            expect(solve(value) == test_cases[value]).to be_truthy
        end
    end
end