ROMAN_NUMERALS = {
    'I' => 1,
    'II' => 2,
    'III' => 3,
    'IV' => 4,
    'V' => 5,
    'IX' => 9,
    'X' => 10,
    'XL' => 40,
    'L' => 50,
    'XC' => 90,
    'C' => 100,
    'CD' => 400,
    'D' => 500,
    'CM' => 900,
    'M' => 1000
}

def solve(roman_input)
    roman_input_copy = roman_input.dup.upcase

    result = []
    digit = 3

    while !roman_input_copy.empty?
        digit -= 1

        if ROMAN_NUMERALS.keys.include?(roman_input_copy[0..digit])
            result << ROMAN_NUMERALS[roman_input_copy[0..digit]]
            roman_input_copy.sub!(roman_input_copy[0..digit], '')
            
            digit = 3
            next
        end
    end

    result.sum
end

puts "III -> #{solve("III")}"
puts "XXX -> #{solve("XXX")}"
puts "CCC -> #{solve("CCC")}"
puts "MMM -> #{solve("MMM")}"

puts

puts "VIII -> #{solve("VIII")}"
puts "LXII -> #{solve("LXII")}"
puts "CLVIII -> #{solve("CLVIII")}"
puts "MCXX -> #{solve("MCXX")}"

puts

puts "IV -> #{solve("IV")}"
puts "IX -> #{solve("IX")}"
puts "XC -> #{solve("XC")}"
